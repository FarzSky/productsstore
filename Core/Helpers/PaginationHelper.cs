﻿using System.Collections.Generic;
using System.Linq;

namespace Core.Helpers
{
    /// <summary>
    /// Helper methods for easier creating paginated results
    /// </summary>
    public static class PaginationHelper
    {
        /// <summary>
        /// Paginate given list.
        /// </summary>
        /// <typeparam name="T">Type of items to paginate.</typeparam>
        /// <param name="items">List of items to paginate.</param>
        /// <param name="page">Page number. Assuming 1 is first page.</param>
        /// <param name="pageSize">Page size.</param>
        /// <returns>Paginated list.</returns>
        public static IEnumerable<T> Paginate<T>(this IEnumerable<T> items, int page, int pageSize)
        {
            page = page <= 0 ? 1 : page;
            return items.Skip((page - 1) * pageSize).Take(pageSize);
        }

        /// <summary>
        /// Calculate first item on page
        /// </summary>
        /// <param name="page">Page number. Assuming 1 is first page.</param>
        /// <param name="pageSize">Page size.</param>
        /// <returns></returns>
        public static int From(int page, int pageSize)
        {
            page = page <= 0 ? 1 : page;
            return ((page - 1) * pageSize) + 1;
        }

        /// <summary>
        /// Calculate last item on page
        /// </summary>
        /// <param name="page">Page number. Assuming 1 is first page.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="total">Total items.</param>
        /// <returns></returns>
        public static int To(int page, int pageSize, int total)
        {
            page = page <= 0 ? 1 : page;
            return (page * pageSize) < total ? (page * pageSize) : total;
        }
    }
}
