﻿using Core.Data;
using Core.Entities;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.UseCases.ProductsUseCase
{
    /// <summary>
    /// Service for providing products.
    /// </summary>
    public interface IGetProductsUseCase
    {
        /// <summary>
        /// Get filtered products.
        /// </summary>
        /// <param name="filter">Optional products filter conditions.</param>
        /// <param name="apikey">Api key to access products db.</param>
        /// <returns>List of products.</returns>
        Task<IEnumerable<Product>> GetProductsAsync(ProductsFilterModel filter, string apikey);

        /// <summary>
        /// Highlighting given list of words in products descriptions.
        /// </summary>
        /// <param name="products">List of products.</param>
        /// <param name="words">List of words.</param>
        /// <returns>List of modified products.</returns>
        IEnumerable<Product> HighlightProducts(IEnumerable<Product> products, IEnumerable<string> words);
    }
}
