﻿using Core.Entities;

using System.Collections.Generic;

namespace Api.Controllers.Products.Models
{
    public class ProductItem
    {
        public string Title { get; set; }
        public int Price { get; set; }
        public IEnumerable<string> Sizes { get; set; }
        public string Description { get; set; }

        public static ProductItem MapFromProduct(Product p)
        {
            return new ProductItem()
            {
                Title = p.Title,
                Price = p.Price,
                Sizes = p.Sizes,
                Description = p.Description
            };
        }
    }
}
