﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Core.Helpers;
using Core.UseCases.ProductsUseCase;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Api.Controllers.Products.Models;

namespace Api.Controllers.Products
{
    /// <summary>
    /// Provides methods that respond to HTTP requests for retrieving products.
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly ILogger<ProductsController> _logger;

        private readonly IGetProductsUseCase _getProductsUseCase;
        private readonly IAnalyticsProductsUseCase _analyticsProductsUseCase;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductsController"/> class.
        /// </summary>
        /// <param name="logger">An instance of <see cref="ILogger"/></param>
        /// <param name="getProductsUseCase">An instance of <see cref="GetProductsUseCase"/></param>
        /// <param name="analyticsProductsUseCase">An instance of <see cref="AnalyticsProductsUseCase"/></param>
        public ProductsController(
            ILoggerFactory loggerFactory,
            IGetProductsUseCase getProductsUseCase,
            IAnalyticsProductsUseCase analyticsProductsUseCase
        )
        {
            _logger = loggerFactory.CreateLogger<ProductsController>();
            _getProductsUseCase = getProductsUseCase;
            _analyticsProductsUseCase = analyticsProductsUseCase;
        }

        /// <summary>
        /// Get products and their analytic
        /// </summary>
        /// <param name="filter">Possible filter conditions and item modifiers.</param>
        [HttpGet]
        [ProducesResponseType(typeof(ProductsResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetProducts([FromQuery] ProductsFilterRequest filter, [FromQuery] string apikey)
        {
            _logger.LogInformation("Get Products Request: " + filter.ToString());
             
            // fetch the products
            var products = await _getProductsUseCase.GetProductsAsync(filter?.MapToFilterModel(), apikey);
            
            // calculate analytics
            var min = _analyticsProductsUseCase.GetMinPrice(products);
            var max = _analyticsProductsUseCase.GetMaxPrice(products);
            var sizes = _analyticsProductsUseCase.GetSizes(products);
            var commonWords = _analyticsProductsUseCase.GetMostCommonWords(products);
            
            // additional adjustments of product descriptions
            if (!(filter.Highlight is null))
            {
                var words = filter.Highlight.Split(",");
                products = _getProductsUseCase.HighlightProducts(products, words);
            }

            // create response of products and analytical data with pagination
            var total = products.Count();
            products = products.Paginate(filter.Page, filter.PageSize);
            var response = new ProductsResponse()
            {
                Products = products.Select(p => ProductItem.MapFromProduct(p)),
                ProductsStats = new ProductsStatistic(min, max, sizes, commonWords),
                Page = filter.Page,
                PageSize = filter.PageSize,
                From = PaginationHelper.From(filter.Page, filter.PageSize),
                To = PaginationHelper.To(filter.Page, filter.PageSize, total),
                Total = total
            };

            return Ok(response);
        }
    }
}
