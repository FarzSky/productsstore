﻿namespace Core.Exceptions
{
    public class BadRequestException : CoreException
    {
        public BadRequestException(string message) : base(400, "BadRequest", message) { }
    }
}
