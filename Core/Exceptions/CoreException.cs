﻿using System;

namespace Core.Exceptions
{
    public class CoreException : Exception
    {
        public int StatusCode { get; }
        public string ErrorCode { get; }

        public CoreException(int statusCode, string errorCode, string message) : base(message ?? "N/A")
        {
            StatusCode = statusCode;
            ErrorCode = errorCode ?? "N/A";
        }
    }

}
