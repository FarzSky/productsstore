﻿using System.Collections.Generic;

namespace Api.Controllers.Products.Models
{
    public class ProductsStatistic
    {
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        public IEnumerable<string> Sizes { get; set; }
        public IEnumerable<string> CommonWords { get; set; }

        public ProductsStatistic(int minPrice, int maxPrice, IEnumerable<string> sizes, IEnumerable<string> commonWords)
        {
            MinPrice = minPrice;
            MaxPrice = maxPrice;
            Sizes = sizes;
            CommonWords = commonWords;
        }
    }
}
