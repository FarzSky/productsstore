﻿namespace Core.Data
{
    public class ProductsFilterModel
    {
        public float? Maxprice { get; set; }
        public string Size { get; set; }
    }
}
