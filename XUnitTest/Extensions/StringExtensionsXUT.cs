﻿using Core.Extensions;

using Xunit;

namespace XUnitTest.Extensions
{
    public class StringExtensionsXUT
    {
        [Theory]
        [InlineData("", "")]
        [InlineData("123", "123")]
        [InlineData("123.", "123")]
        [InlineData(".1,2>3!", "12>3")]
        public void ClearPunctuations_success(string input, string output)
        {
            // act
            var result = input.ClearPunctuations();

            // assert
            Assert.Equal(output, result);
        }
    }
}
