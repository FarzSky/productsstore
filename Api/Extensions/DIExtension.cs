﻿using Core.UseCases.ProductsUseCase;

using Microsoft.Extensions.DependencyInjection;

namespace Api.Extensions
{
    public static class DIExtension
    {
        /// <summary>
        /// Helper for injecting required use case services
        /// </summary>
        /// <param name="services">Service collection</param>
        /// <returns>Service collection</returns>
        public static IServiceCollection AddUseCases(this IServiceCollection services)
        {
            services.AddScoped<IGetProductsUseCase, GetProductsUseCase>();
            services.AddScoped<IAnalyticsProductsUseCase, AnalyticsProductsUseCase>();

            return services;
        }

        /// <summary>
        /// Helper for injecting required data providers (db providers, network providers...)
        /// </summary>
        /// <param name="services">Service collection</param>
        /// <returns>Service collection</returns>
        public static IServiceCollection AddDataProviders(this IServiceCollection services)
        {

            return services;
        }
    }
}
