﻿namespace Core.Data
{
    public class ApiKeys
    {
        public string Primary { get; set; }
        public string Secondary { get; set; }
    }
}
