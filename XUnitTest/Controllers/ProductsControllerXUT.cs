﻿using Api.Controllers.Products;
using Api.Controllers.Products.Models;

using Core.Data;
using Core.Exceptions;
using Core.UseCases.ProductsUseCase;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;

using Moq;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;

using XUnitTest.Data;

namespace XUnitTest.Controllers
{
    public class ProductsControllerXUT
    {
        private readonly Mock<IGetProductsUseCase> _getProductsUseCaseMock;
        private readonly Mock<IAnalyticsProductsUseCase> _analyticsProductsUseCaseMock;

        public ProductsControllerXUT()
        {
            _getProductsUseCaseMock = new Mock<IGetProductsUseCase>();
            _analyticsProductsUseCaseMock = new Mock<IAnalyticsProductsUseCase>();
        }

        /// <summary>
        /// Test GetProducts without filters or adjustments. Should return all products successfully.
        /// </summary>
        [Fact]
        public async Task GetProducts_nofilters_success()
        {
            // setup
            var filter = new ProductsFilterRequest();
            var fakeProducts = ProductsFake.AllProducts();
            var fakeSizes = new List<string>() { "small", "medium", "large" };
            var fakeWords = new List<string>() { "smooth", "silky", "blue", "black", "green", "but", "expensive" };

            _getProductsUseCaseMock.Setup(x => x.GetProductsAsync(It.IsAny<ProductsFilterModel>(), It.IsAny<string>()))
                .ReturnsAsync(fakeProducts);

            _analyticsProductsUseCaseMock.Setup(x => x.GetMinPrice(fakeProducts))
                .Returns(2);
            _analyticsProductsUseCaseMock.Setup(x => x.GetMaxPrice(fakeProducts))
                .Returns(25);
            _analyticsProductsUseCaseMock.Setup(x => x.GetSizes(fakeProducts))
                .Returns(fakeSizes);
            _analyticsProductsUseCaseMock.Setup(x => x.GetMostCommonWords(fakeProducts))
                .Returns(fakeWords);

            // act
            var productController = new ProductsController(new NullLoggerFactory(), _getProductsUseCaseMock.Object, _analyticsProductsUseCaseMock.Object);
            var actionResult = await productController.GetProducts(filter, "");

            // assert
            var productsResponse = ((ObjectResult)actionResult).Value as ProductsResponse;
            Assert.Equal((int)System.Net.HttpStatusCode.OK, (actionResult as OkObjectResult).StatusCode);
            Assert.Equal(fakeProducts.Count(), (((ObjectResult)actionResult).Value as ProductsResponse).Products.Count());
            Assert.Equal(2, productsResponse.ProductsStats.MinPrice);
            Assert.Equal(25, productsResponse.ProductsStats.MaxPrice);
            Assert.True(fakeSizes.Intersect(productsResponse.ProductsStats.Sizes).Count() == fakeSizes.Count());
            Assert.True(fakeWords.Intersect(productsResponse.ProductsStats.CommonWords).Count() == fakeWords.Count());
        }

        /// <summary>
        /// Test GetProducts without filters or adjustments. Should throw AuthorizationException becuase of unauthorized access.
        /// </summary>
        [Fact]
        public async Task GetProducts_nofilters_unauthorized_fail()
        {
            // setup
            var filter = new ProductsFilterRequest();

            _getProductsUseCaseMock.Setup(x => x.GetProductsAsync(It.IsAny<ProductsFilterModel>(), "unauthorized-apikey"))
                .ThrowsAsync(new AuthorizationException("Unauthorized"));

            // act
            var productController = new ProductsController(new NullLoggerFactory(), _getProductsUseCaseMock.Object, _analyticsProductsUseCaseMock.Object);

            // assert
            await Assert.ThrowsAsync<AuthorizationException>(() => productController.GetProducts(filter, "unauthorized-apikey"));
        }
    }
}
