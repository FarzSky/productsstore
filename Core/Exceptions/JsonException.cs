﻿namespace Core.Exceptions
{
    public class JsonConversionException : CoreException
    {
        public JsonConversionException(string message) : base(400, "JsonConversion", message) { }
    }
}
