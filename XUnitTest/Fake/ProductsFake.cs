﻿using Core.Entities;

using System.Collections.Generic;

namespace XUnitTest.Data
{
    public static class ProductsFake
    {
        public static IEnumerable<Product> AllProducts()
        {
            return new List<Product>()
            {
                new Product()
                {
                    Title = "p1",
                    Price = 2,
                    Sizes = new List<string>() { "small" },
                    Description = "This p is nice smooth silky and blue."
                },
                new Product()
                {
                    Title = "p2",
                    Price = 4,
                    Sizes = new List<string>() { "small", "medium" },
                    Description = "This p is nice smooth silky and black."
                },
                new Product()
                {
                    Title = "p3",
                    Price = 10,
                    Sizes = new List<string>() {  },
                    Description = "This p is nice smooth silky and blue and green."
                },
                new Product()
                {
                    Title = "p4",
                    Price = 15,
                    Sizes = new List<string>() { "large" },
                    Description = ""
                },
                new Product()
                {
                    Title = "p5",
                    Price = 25,
                    Sizes = new List<string>() { "medium", "large" },
                    Description = "This p is nice but expensive."
                }
            };
        }

        public static IEnumerable<Product> GetNProducts(int n)
        {
            var result = new List<Product>();
            for (int i=1; i<=n; i++)
            {
                result.Add(new Product()
                {
                    Title = $"product {i}",
                    Price = i,
                    Sizes = new List<string>(),
                    Description = $"product {i} description "
                });
            }

            return result;
        }
    }
}
