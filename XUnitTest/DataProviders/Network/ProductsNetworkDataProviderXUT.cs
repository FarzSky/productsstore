﻿using Core.Data;

using Infrastructure.Configurations;
using Infrastructure.DataProviders.Network;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;

using Moq;
using Moq.Protected;

using Newtonsoft.Json;

using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

using Xunit;

using XUnitTest.Data;

namespace XUnitTest.DataProviders.Network
{
    public class ProductsNetworkDataProviderXUT
    {
        private readonly Mock<HttpMessageHandler> _httpMessageHandlerMock;

        public ProductsNetworkDataProviderXUT()
        {
            _httpMessageHandlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
        }

        [Fact]
        public async Task GetAllProducts_success()
        {
            // setup
            var fakeProducts = ProductsFake.AllProducts();
            var fakeApiProducts = new ApiProducts()
            {
                Products = fakeProducts,
                ApiKeys = new ApiKeys()
                {
                    Primary = ""
                }
            };

            // mock the http handler response
            _httpMessageHandlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                // prepare the expected response of the mocked http call
                .ReturnsAsync(new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(JsonConvert.SerializeObject(fakeApiProducts)),
                })
                .Verifiable();

            // use real http client with mocked handler here
            var httpClient = new HttpClient(_httpMessageHandlerMock.Object)
            {
                BaseAddress = new Uri("http://test.com/"),
            };

            // act
            var dp = new ProductsNetworkDataProvider(
                new NullLoggerFactory(),
                httpClient, 
                Options.Create(new ApiConfiguration() 
                { 
                    MockyUrl = "http://test.com/" 
                })
            );
            var responseProducts = await dp.GetAllProducts("");

            // assert
            Assert.True(responseProducts.Count() == fakeProducts.Count());
        }

        [Fact]
        public async Task GetProducts_nofilters_success()
        {
            // setup
            var fakeProducts = ProductsFake.AllProducts();
            var fakeApiProducts = new ApiProducts()
            {
                Products = fakeProducts,
                ApiKeys = new ApiKeys()
                {
                    Primary = ""
                }
            };
            var fakeFilter = new ProductsFilterModel();

            // mock the http handler response
            _httpMessageHandlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                // prepare the expected response of the mocked http call
                .ReturnsAsync(new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(JsonConvert.SerializeObject(fakeApiProducts)),
                })
                .Verifiable();

            // use real http client with mocked handler here
            var httpClient = new HttpClient(_httpMessageHandlerMock.Object)
            {
                BaseAddress = new Uri("http://test.com/"),
            };

            // act
            var dp = new ProductsNetworkDataProvider(
                new NullLoggerFactory(),
                httpClient,
                Options.Create(new ApiConfiguration()
                {
                    MockyUrl = "http://test.com/"
                })
            );
            var result = await dp.GetProducts(fakeFilter, "");

            // assert
            Assert.True(result.Count() == fakeProducts.Count());
        }
    }
}
