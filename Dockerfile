FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY ["Api/Api.csproj", "Api/"]
COPY ["Infrastructure/Infrastructure.csproj", "Infrastructure/"]
COPY ["Core/Core.csproj", "Core/"]
COPY ["XUnitTest/XUnitTest.csproj", "XUnitTest/"]
RUN dotnet restore "Api/Api.csproj"
RUN dotnet restore "Infrastructure/Infrastructure.csproj"
RUN dotnet restore "Core/Core.csproj"
RUN dotnet restore "XUnitTest/XUnitTest.csproj"

COPY . .
RUN dotnet build "Api/Api.csproj" -c Release -o /app/build

# run the unit tests
FROM build AS test
WORKDIR XUnitTest
RUN dotnet test --logger:trx

FROM build AS publish
RUN dotnet publish "Api/Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Api.dll"]