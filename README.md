# Products store

Implementation made using ASP.NET Core 3.1. 

Dockerized (both linux and windows containers working) and can be easy started with docker-compose.

## How to run

run "docker-compose up"

open browser on http://localhost:5555/swagger

## Details and Thoughts
For simplicity of demonstration, containers are exposing http instead of https (easy testing, no need for ssl certificates installation...)
For the whole project, took me around 6-7 hours. Little more then expected.
Reason: wanted to use this assignment as opportunity for experimenting with "new" architecture -> Clean Architecture presented by Uncle Bob.
As this was my first try of this architecture, some things probably were not implemented in best possible way, i had few question marks 
over my head which could not answer to myself with the right conclusion for now. Like keeping dtos in the web level, 
which is helping with SOLID principles, but knows to give headaches in implementation in some cases. Then not having interfaces for
use cases was strange to me, because that makes testing harder (no interface to mock which Moq is "kinda" requires). 
Exception recommended to be defined on specific bussiness case level, which i think could be more generalized and no need for that.
Few more...

Unit tests were recommended to be in every circle separate, but for simplicity i have created single xUnit project so that all
unit tests can be in one place.

I did create all positive tests, and just a 1 sample of negative (XUnitTest.Controllers.ProductsControllerXUT.GetProducts_nofilters_unauthorized_fail)
just to show an example of negative unit test as well. I didnt wanted to loose more time on every possible use case that could happen
on the methods, already was hoping that the goal was to show that i can write some unit tests in general.
Same for some algorithm implementations, like word parsers and word highlighter, didnt wanted to loose too much time on 
perfect parsers.

Some things i have may understood wrong, but i tried to implement bussiness logic as good as possible from what i understood.

From security topics, wasnt completely sure what to cover, i know there is some solutions for resolving XSS attacks with headers, 
parsing bodyes (input request, input response e.g. mocky.io) for invalid chars and possible code injections. Mostly i would
solve those on load balancer with firewall, which would take actually a lot of performance resources on api as a negative side.
But i have skipped those implementations in code, because it would took me too long for searchig for the right solutions, which i also hoped would be enough to cover
would be enough of showing that i am aware of those problems.