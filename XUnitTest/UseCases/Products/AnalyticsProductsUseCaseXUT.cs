﻿using Core.UseCases.ProductsUseCase;

using System.Collections.Generic;
using System.Linq;

using Xunit;

using XUnitTest.Data;

namespace XUnitTest.UseCases.Products
{
    public class AnalyticsProductsUseCaseXUT
    {
        [Fact]
        public void GetMinPrice_success()
        {
            // setup
            var fakeProducts = ProductsFake.AllProducts();

            // act
            var uc = new AnalyticsProductsUseCase();
            var result = uc.GetMinPrice(fakeProducts);

            // assert
            Assert.Equal(2, result);
        }

        [Fact]
        public void GetMaxPrice_success()
        {
            // setup
            var fakeProducts = ProductsFake.AllProducts();

            // act
            var uc = new AnalyticsProductsUseCase();
            var result = uc.GetMaxPrice(fakeProducts);

            // assert
            Assert.Equal(25, result);
        }

        [Fact]
        public void GetSizes_success()
        {
            // setup
            var fakeProducts = ProductsFake.AllProducts();
            var fakeSizes = new List<string>() { "small", "medium", "large" };

            // act
            var uc = new AnalyticsProductsUseCase();
            var result = uc.GetSizes(fakeProducts);

            // assert
            Assert.True(fakeSizes.Intersect(result).Count() == fakeSizes.Count());
        }

        [Fact]
        public void GetMostCommonWords_success()
        {
            // setup
            var fakeProducts = ProductsFake.AllProducts();
            var fakeWords = new List<string>() { "smooth", "silky", "blue", "black", "green", "but", "expensive" };

            // act
            var uc = new AnalyticsProductsUseCase();
            var result = uc.GetMostCommonWords(fakeProducts);

            // assert
            Assert.True(fakeWords.Intersect(result).Count() == fakeWords.Count());
        }
    }
}
