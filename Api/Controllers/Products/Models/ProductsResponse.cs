﻿using System.Collections.Generic;

using Api.Models;

namespace Api.Controllers.Products.Models
{
    public class ProductsResponse : PaginationResponse
    {
        public IEnumerable<ProductItem> Products { get; set; }
        public ProductsStatistic ProductsStats { get; set; }
    }
}
