﻿using System.Linq;

namespace Core.Extensions
{
    public static class StringExtension
    {
        public static string ClearPunctuations(this string input)
        {
            return new string(input.Where(c => !char.IsPunctuation(c)).ToArray());
        }
    }
}
