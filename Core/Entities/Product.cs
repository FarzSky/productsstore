﻿using System.Collections.Generic;

namespace Core.Entities
{
    public class Product
    {
        public string Title { get; set; }
        public int Price { get; set; }
        public IEnumerable<string> Sizes { get; set; }
        public string Description { get; set; }
    }
}
