﻿using Core.Adapters;
using Core.Data;
using Core.Entities;
using Core.Exceptions;

using Infrastructure.Configurations;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Infrastructure.DataProviders.Network
{
    /// <summary>
    /// Products data provider fetching them from the API.
    /// </summary>
    public class ProductsNetworkDataProvider : IProductsRepository
    {
        private readonly HttpClient _client;
        private readonly string _mockyUrl;
        private readonly ILogger<ProductsNetworkDataProvider> _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductsNetworkDataProvider"/> class.
        /// </summary>
        /// <param name="client">An instance of <see cref="HttpClient"/></param>
        /// <param name="apiConfiguration">Api configuration object holding api uris./></param>
        public ProductsNetworkDataProvider(
            ILoggerFactory loggerFactory,
            HttpClient client,
            IOptions<ApiConfiguration> apiConfiguration
        )
        {
            _client = client;
            _logger = loggerFactory.CreateLogger<ProductsNetworkDataProvider>();
            _mockyUrl = apiConfiguration?.Value?.MockyUrl ?? throw new ArgumentNullException("Missing mocky url.");
        }

        /// <inheritdoc cref="IProductsRepository.GetAllProducts()"/>
        public async Task<IEnumerable<Product>> GetAllProducts(string apikey)
        {
            var uri = new Uri(_mockyUrl);

            // api call
            string productsResponse;
            try
            {
                var response = await _client.GetAsync(uri);
                if (!response.IsSuccessStatusCode)
                {
                    throw new BadRequestException("Failed to fetch products.");
                }

                productsResponse = await response.Content.ReadAsStringAsync();
                _logger.LogInformation("Products response: " + productsResponse);
            }
            catch (HttpRequestException ex)
            {
                throw new BadRequestException("Failed to fetch products: " + ex.Message);
            }

            // products deserialization
            ApiProducts apiProducts = null;
            try
            {
                apiProducts = JsonConvert.DeserializeObject<ApiProducts>(productsResponse);
            }
            catch (Exception ex)
            {
                throw new JsonConversionException("Failed to convert products from response: " + ex.Message);
            }

            if (apiProducts?.ApiKeys?.Primary != apikey && apiProducts?.ApiKeys?.Secondary != apikey)
                throw new AuthorizationException("User is not authorized to access products.");

            return apiProducts?.Products;
        }

        /// <inheritdoc cref="IProductsRepository.GetProducts(ProductsFilterModel)"/>
        public async Task<IEnumerable<Product>> GetProducts(ProductsFilterModel filter, string apikey)
        {
            // fetch all the products first, and then filter them, if filters exists.
            var products = await GetAllProducts(apikey);
            if (filter is null)
                return products;

            if (!(filter.Maxprice is null))
            {
                products = products.Where(p => p.Price < filter.Maxprice);
            }

            if (!string.IsNullOrEmpty(filter.Size))
            {
                products = products.Where(p => p.Sizes.Any(s => s.ToLowerInvariant() == filter.Size.ToLowerInvariant()));
            }

            return products;
        }
    }
}
