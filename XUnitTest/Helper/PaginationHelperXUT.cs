﻿using Core.Helpers;

using System.Linq;

using Xunit;

using XUnitTest.Data;

namespace XUnitTest.Helper
{
    public class PaginationHelperXUT
    {
        [Theory]
        [InlineData(-1, 10, 10)]
        [InlineData(1, 10, 10)]
        [InlineData(10, 10, 8)]
        public void Paginate_success(int page, int pagesize, int expectedItems)
        {
            // setup
            var products = ProductsFake.GetNProducts(98);
            
            // act
            var result = PaginationHelper.Paginate(products, page, pagesize);

            // assert
            Assert.True(result.Count() == expectedItems);
            Assert.Contains(result.Select(s => s.Price), p => p > ((page - 1) * pagesize) || p <= (page * pagesize));
        }

        [Theory]
        [InlineData(0, 10, 1)]
        [InlineData(5, 10, 41)]
        public void From_success(int page, int pageSize, int expected)
        {
            // setup
            var products = ProductsFake.GetNProducts(100);

            // act
            var result = PaginationHelper.From(page, pageSize);

            // assert
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(0, 10, 100, 10)]
        [InlineData(5, 10, 100, 50)]
        [InlineData(10, 10, 100, 100)]
        [InlineData(10, 10, 99, 99)]
        public void To_success(int page, int pageSize, int total, int expected)
        {
            // setup
            var products = ProductsFake.GetNProducts(100);

            // act
            var result = PaginationHelper.To(page, pageSize, total);

            // assert
            Assert.Equal(expected, result);
        }
    }
}
