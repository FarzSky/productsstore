﻿using Core.Data;
using Core.Entities;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Adapters
{
    public interface IProductsRepository
    {
        /// <summary>
        /// Get all products.
        /// </summary>
        /// <param name="apikey">Api key for products access.<./param>
        /// <returns>List of products.</returns>
        Task<IEnumerable<Product>> GetAllProducts(string apikey);

        /// <summary>
        /// Get products filtered by optional conditions.
        /// </summary>
        /// <param name="filter">Optional filter conditions of products<./param>
        /// <param name="apikey">Api key for products access.<./param>
        /// <returns>Filtered list of products.</returns>
        Task<IEnumerable<Product>> GetProducts(ProductsFilterModel filter, string apikey);
    }
}
