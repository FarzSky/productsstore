﻿using Core.Adapters;
using Core.Data;
using Core.UseCases.ProductsUseCase;

using Moq;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;

using XUnitTest.Data;

namespace XUnitTest.UseCases.Products
{
    public class GetProductsUseCaseXUT
    {
        private readonly Mock<IProductsRepository> _productsRepositoryMock;

        public GetProductsUseCaseXUT()
        {
            _productsRepositoryMock = new Mock<IProductsRepository>();
        }

        [Fact]
        public async Task GetProductsAsync_nofilter_success()
        {
            // setup
            var filter = new ProductsFilterModel();
            var fakeProducts = ProductsFake.AllProducts();

            _productsRepositoryMock.Setup(x => x.GetProducts(filter, It.IsAny<string>()))
                .ReturnsAsync(fakeProducts);

            // act
            var productsUC = new GetProductsUseCase(_productsRepositoryMock.Object);
            var products = await productsUC.GetProductsAsync(filter, "");

            // assert
            Assert.NotNull(products);
            Assert.NotEmpty(products);
        }

        [Fact]
        public void HighlightProducts_success()
        {
            // setup
            var fakeProducts = ProductsFake.AllProducts();
            var highlightWords = new List<string>() { "blue", "black", "green" };

            // act
            var productsUC = new GetProductsUseCase(_productsRepositoryMock.Object);
            var result = productsUC.HighlightProducts(fakeProducts, highlightWords);

            // assert
            var joinedDesc = string.Join(" ", result.Select(p => p.Description));
            Assert.True(joinedDesc.Contains("<em>blue</em>") && joinedDesc.Contains("<em>black</em>") && joinedDesc.Contains("<em>green</em>"));
        }
    }
}
