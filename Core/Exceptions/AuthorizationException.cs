﻿namespace Core.Exceptions
{
    public class AuthorizationException : CoreException
    {
        public AuthorizationException(string message) : base(401, "Unauthorized", message) { }
    }
}
