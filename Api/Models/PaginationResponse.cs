﻿namespace Api.Models
{
    public class PaginationResponse
    {
        public int Page { get; set; } = 1;
        public int PageSize { get; set; } = int.MaxValue;
        public int From { get; set; } = 1;
        public int To { get; set; } = int.MaxValue;
        public int Total { get; set; }
    }
}
