﻿using Core.Entities;
using Core.Extensions;

using System.Collections.Generic;
using System.Linq;

namespace Core.UseCases.ProductsUseCase
{
    /// <inheritdoc cref="IGetProductsUseCase.GetProductsAsync(ProductsFilterModel)"/>
    public class AnalyticsProductsUseCase : IAnalyticsProductsUseCase
    {
        /// <inheritdoc cref="IAnalyticsProductsUseCase.GetMinPrice(IEnumerable{Product})"/>
        public int GetMinPrice(IEnumerable<Product> products)
        {
            if (products is null || products.Count() == 0)
                return 0;

            return products.Select(p => p.Price)?.Min() ?? 0;
        }

        /// <inheritdoc cref="IAnalyticsProductsUseCase.GetMaxPrice(IEnumerable{Product})"/>
        public int GetMaxPrice(IEnumerable<Product> products)
        {
            if (products is null || products.Count() == 0)
                return 0;

            return products.Select(p => p.Price).Max();
        }

        /// <inheritdoc cref="IAnalyticsProductsUseCase.GetSizes(IEnumerable{Product})"/>
        public IEnumerable<string> GetSizes(IEnumerable<Product> products)
        {
            if (products is null || products.Count() == 0)
                return new List<string>();

            return products.SelectMany(p => p.Sizes).Distinct();
        }

        /// <inheritdoc cref="IAnalyticsProductsUseCase.GetMostCommonWords(IEnumerable{Product})"/>
        public IEnumerable<string> GetMostCommonWords(IEnumerable<Product> products)
        {
            if (products is null || products.Count() == 0)
                return new List<string>();

            // counting words with map as <word, count>
            var wordMap = new Dictionary<string, int>();
            foreach (var p in products)
            {
                if (string.IsNullOrEmpty(p.Description))
                    continue;

                foreach (var word in p.Description.Split(" "))
                {
                    var w = word.ClearPunctuations();
                    if (wordMap.ContainsKey(w))
                    {
                        wordMap[w]++;
                    }
                    else
                    {
                        wordMap.Add(w, 1);
                    }
                }
            }

            // order words by counts, geting top 10 words max, with skipping first 5 (spam words or similar)
            return wordMap.ToList()
                .OrderByDescending(kv => kv.Value)
                .Skip(5)
                .Take(10)
                .Select(kv => kv.Key);
        }
    }
}
