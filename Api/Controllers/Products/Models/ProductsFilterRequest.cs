﻿using Core.Data;

using System.Text;

using Api.Models;

namespace Api.Controllers.Products.Models
{
    public class ProductsFilterRequest : PaginationRequest
    {
        public float? Maxprice { get; set; }
        public string Size { get; set; }
        public string Highlight { get; set; }

        public ProductsFilterModel MapToFilterModel()
        {
            return new ProductsFilterModel()
            {
                Maxprice = Maxprice,
                Size = Size
            };
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (!(Maxprice is null))
                sb.Append($"{nameof(Maxprice)}: {Maxprice}");
            if (!(Size is null))
                sb.Append($"{nameof(Size)}: {Size}");
            if (!(Highlight is null))
                sb.Append($"{nameof(Highlight)}: {Highlight}");
            return "{ " + sb.ToString() + " }";
        }
    }
}
