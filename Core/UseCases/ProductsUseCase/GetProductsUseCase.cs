﻿using Core.Adapters;
using Core.Data;
using Core.Entities;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.UseCases.ProductsUseCase
{
    /// <inheritdoc cref="IGetProductsUseCase"/>
    public class GetProductsUseCase : IGetProductsUseCase
    {
        private readonly IProductsRepository _productsRepository;

        /// <summary>
        /// Creating an instance of the <see cref="GetProductsUseCase"/>
        /// </summary>
        /// <param name="productsRepository">An instance of <see cref="IProductsRepository"/></param>
        public GetProductsUseCase(IProductsRepository productsRepository)
        {
            _productsRepository = productsRepository;
        }

        /// <inheritdoc cref="IGetProductsUseCase.GetProductsAsync(ProductsFilterModel)"/>
        public async Task<IEnumerable<Product>> GetProductsAsync(ProductsFilterModel filter, string apikey)
        {
            return await _productsRepository.GetProducts(filter, apikey);
        }

        /// <inheritdoc cref="IGetProductsUseCase.HighlightProducts(IEnumerable{Product}, IEnumerable{string})"/>
        public IEnumerable<Product> HighlightProducts(IEnumerable<Product> products, IEnumerable<string> words)
        {
            if (products is null)
                throw new ArgumentNullException("Products not provided.");

            if (words is null)
                throw new ArgumentNullException("Words not provided");

            foreach (var p in products)
            {
                foreach (var word in words)
                {
                    p.Description = p.Description?.Replace($" {word}", $" <em>{word}</em>");
                }
            }

            return products;
        }
    }
}
