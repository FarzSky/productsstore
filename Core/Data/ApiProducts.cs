﻿using Core.Entities;

using System.Collections.Generic;

namespace Core.Data
{
    public class ApiProducts
    {
        public IEnumerable<Product> Products { get; set; }
        public ApiKeys ApiKeys { get; set; }
    }
}
