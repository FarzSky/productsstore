﻿using Core.Exceptions;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

using System;
using System.Threading.Tasks;

namespace Infrastructure.Middlewares
{
    /// <summary>
    /// Provides methods, that are intersecting exceptions and trying to log them in formated way and returns http responses in formated way with proper error codes.
    /// </summary>
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionMiddleware> _logger;

        public ExceptionMiddleware(RequestDelegate next, ILoggerFactory factory)
        {
            _next = next;
            _logger = factory?.CreateLogger<ExceptionMiddleware>() ?? throw new ArgumentNullException(nameof(factory));
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        /// <summary>
        /// Handle intersected exception. Special format of messages is used in case of intersecting user handled exceptions.
        /// For unhandled exceptions, only message is logged.
        /// </summary>
        /// <param name="context">Http Context</param>
        /// <param name="ex">Intersected exception</param>
        private async Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            int statusCode;
            string errorCode;
            string message;

            if (ex is CoreException cex)
            {
                statusCode = cex.StatusCode;
                errorCode = cex.ErrorCode;
                message = cex.Message;
            }
            else
            {
                statusCode = 500;
                errorCode = "InternalError";
                message = "Ops. Something went wrong.";
            }

            _logger.LogError(ex.ToString());
            
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = statusCode;
            await context.Response.WriteAsync(JsonConvert.SerializeObject(
                new {
                    errorCode,
                    message
                }
            ));
        }

    }
}
