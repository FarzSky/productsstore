﻿using Core.Entities;

using System.Collections.Generic;

namespace Core.UseCases.ProductsUseCase
{
    /// <summary>
    /// Provides methods for calculating products analytics
    /// </summary>
    public interface IAnalyticsProductsUseCase
    {
        /// <summary>
        /// Get minimal price of given products.
        /// </summary>
        /// <param name="products">List of products</param>
        /// <returns>Minimal price</returns>
        int GetMinPrice(IEnumerable<Product> products);

        /// <summary>
        /// Get max price of given products.
        /// </summary>
        /// <param name="products">List of products.</param>
        /// <returns>Max price</returns>
        int GetMaxPrice(IEnumerable<Product> products);

        /// <summary>
        /// Get all unique sizes of all given products.
        /// </summary>
        /// <param name="products">List of products.</param>
        /// <returns>List of unique sizes.</returns>
        IEnumerable<string> GetSizes(IEnumerable<Product> products);

        /// <summary>
        /// Get top 10 (skip top 5) most common words in product descriptions
        /// </summary>
        /// <param name="products">List of products</param>
        /// <returns>List of words</returns>
        IEnumerable<string> GetMostCommonWords(IEnumerable<Product> products);
    }
}
